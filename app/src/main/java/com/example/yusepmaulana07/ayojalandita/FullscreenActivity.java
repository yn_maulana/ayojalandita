package com.example.yusepmaulana07.ayojalandita;

import android.animation.ValueAnimator;
import android.annotation.SuppressLint;
import android.content.DialogInterface;
import android.content.Intent;
import android.media.Image;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.os.Handler;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.LinearInterpolator;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.util.Random;


public class FullscreenActivity extends AppCompatActivity {

    int positionDita = 200;
    Boolean animationYopi = false;
    float randFloat= 0.0f;
    int highScore =0;
    int score = 0;
    TextView textView;
    Boolean getAa = false;
    Boolean tutorial = true;
    Boolean check = true;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_fullscreen);

        Button next = (Button) findViewById(R.id.button2);
        final Button buttonPlay = (Button) findViewById(R.id.buttonPlay);
        buttonPlay.setVisibility(View.VISIBLE);
        final Button prev = (Button) findViewById(R.id.button3);

            next.setOnTouchListener(new View.OnTouchListener() {
                @Override
                public boolean onTouch(View view, MotionEvent motionEvent) {
                    if (positionDita>-10){
                        Button dita = (Button) findViewById(R.id.Dita);
                        positionDita=positionDita-100;
                        dita.setX( positionDita);
                        ImageView imgDita = (ImageView) findViewById(R.id.imageView2);
                        imgDita.setX(positionDita);
                    }

                    return false;
                }
            });

            prev.setOnTouchListener(new View.OnTouchListener() {
                @Override
                public boolean onTouch(View view, MotionEvent motionEvent) {
                    if (positionDita<800){
                        Button dita = (Button) findViewById(R.id.Dita);
                        positionDita=positionDita+100;
                        dita.setX( positionDita);
                        ImageView imgDita = (ImageView) findViewById(R.id.imageView2);
                        imgDita.setX(positionDita);
                    }
                    return false;
                }
            });


        final ImageView backgroundOne = (ImageView) findViewById(R.id.imageView);
        final ImageView aaIcon = (ImageView) findViewById(R.id.aaIcon);

        Button dita = (Button) findViewById(R.id.Dita);
        final Random random = new Random();
        randFloat = random.nextFloat()*900.0f;
        backgroundOne.setX(randFloat);

        textView = (TextView) findViewById(R.id.scoreText);

        final ValueAnimator animator = ValueAnimator.ofFloat(0.0f, 9.0f);
        animator.setRepeatCount(ValueAnimator.INFINITE);
        animator.setInterpolator(new LinearInterpolator());
        animator.setDuration(4000);
        animator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            @Override
            public void onAnimationUpdate(final ValueAnimator animation) {
                final float progress = (float) animation.getAnimatedValue();
                final float height = backgroundOne.getHeight();
                final float translationY = height * progress;
                backgroundOne.setTranslationY(translationY);
                if (progress < 3.0f){
                    aaIcon.setTranslationY(translationY);
                }

                if (progress >= 4.0f && progress <= 6.0f ){
                    if (Math.abs(positionDita-randFloat)<200 || getAa){
                        animator.cancel();
                        if (score>highScore){
                            highScore = score;
                        }

                        DialogInterface.OnClickListener dialogClickListener = new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                switch (which){
                                    case DialogInterface.BUTTON_POSITIVE:
                                        score = 0;
                                        getAa = false;
                                        aaIcon.setVisibility(View.INVISIBLE);
                                        backgroundOne.setVisibility(View.VISIBLE);
                                        animation.setDuration(4000);
                                        break;

                                    case DialogInterface.BUTTON_NEGATIVE:
                                        finishAffinity();
                                        break;
                                }
                            }
                        };

                        AlertDialog.Builder builder = new AlertDialog.Builder(FullscreenActivity.this);
                        if (getAa){
                            builder.setMessage("Yeayyy ...Kamu menang ...\n\n   Score : "+score+" \n   High score : "+highScore).setPositiveButton("Main Lagi", dialogClickListener)
                                    .setNegativeButton("Keluar", dialogClickListener).show();
                        }else {
                            builder.setMessage("Yah kalah ...\n\n   Score : "+score+" \n   High score : "+highScore).setPositiveButton("Coba Lagi", dialogClickListener)
                                    .setNegativeButton("Keluar", dialogClickListener).show();
                        }
                        buttonPlay.setVisibility(View.VISIBLE);
                    }

                }else if (progress >= 8.0f && check  ){
                    textView.setText(String.valueOf(score));

                    randFloat = random.nextFloat()*900.0f;
                    backgroundOne.setX(randFloat);

                    score+=10;

                    if (score>=200){
                        aaIcon.setVisibility(View.VISIBLE);
                        backgroundOne.setVisibility(View.INVISIBLE);
                        animation.setDuration(20000);
                        getAa =true;
                    }

                    check = false;
                }else if (progress < 3.0f && !check){
                    check = true;
                }
            }

        });

        dita.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (animationYopi){
                    animator.end();
                    animationYopi = false;
                }else{
                    animator.start();
                    animationYopi = true;
                }
            }
        });

        buttonPlay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (tutorial){
                    tutorial=false;
                    Intent myIntent = new Intent(view.getContext(), HowToPlayActivity.class);
                    startActivityForResult(myIntent, 0);
                }else{
                    if(!animationYopi){
                        animator.start();
                        buttonPlay.setVisibility(View.INVISIBLE);
                    }
                }
            }
        });
    }
}
